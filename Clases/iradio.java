
/**
 *douglas.java 
 * Interfaz que tiene los siguientes métodos
 * @author Paul Belches
 * @author Javier Carpio
 * @since 14/01/18
 */
public interface iradio {

    /**
     * Método de encendido y/o apagado para la radio.
     */
    public abstract void onOff();

    /**
     * Método para cambiar entre frecuencia Am/FM.
     * @return el valor de la nueva estación despúes del cambio
     */
    public abstract float Switch();

    /**
     * Método para cambiar a la siguiente frecuencia. 
     * @param a frecuencia actual
     * @return siguiente frecuencia
     */
    public abstract float siguiente(float a);

    /**
     * Método para cambiar a la frecuencia anterior.
     * @param a frecuencia actual
     * @return frecuencia anterior
     */
    public abstract float anterior(float a);

    /**
     * Método para guardar una estación en alguno de los botones.
     * @param e Estación a guardar
     * @param b Botón en donde se va a guardar 
     */
    public abstract void guardar(float e,int b);

    /**
     * Selecciónar una de las estaciones que se guardo.
     * @param b Boton de donde se quiere obtener la estación
     * @return La estación guardada en el botón
     */
    public abstract float seleccionarFav(int b);
}
