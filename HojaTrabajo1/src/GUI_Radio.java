
import javax.swing.JOptionPane;

/**
 * @author Paul Belches
 * @author Javier Carpio
 * @since 14/01/18
 */
public class GUI_Radio extends javax.swing.JFrame {

    private douglas miRadio;
    private float frecuencia;
    private String estacion;
    private String texto;
    public GUI_Radio() {
        initComponents();
        
        frecuencia = (float)87.9;
        texto = String.valueOf(frecuencia);
        estacion = "FM";
        
        miRadio = new Radio() {};
        
        lblPantalla.setText(texto);
        lblFrecuencia.setText(estacion);
        pnlRadio.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnEncender = new javax.swing.JButton();
        btnApagar = new javax.swing.JButton();
        pnlRadio = new javax.swing.JPanel();
        btnAvanzar = new javax.swing.JButton();
        B12 = new javax.swing.JButton();
        btnRetroceder = new javax.swing.JButton();
        B10 = new javax.swing.JButton();
        B11 = new javax.swing.JButton();
        B8 = new javax.swing.JButton();
        B9 = new javax.swing.JButton();
        B7 = new javax.swing.JButton();
        B4 = new javax.swing.JButton();
        B3 = new javax.swing.JButton();
        B2 = new javax.swing.JButton();
        B1 = new javax.swing.JButton();
        B6 = new javax.swing.JButton();
        B5 = new javax.swing.JButton();
        lblPantalla = new javax.swing.JLabel();
        btnFM = new javax.swing.JButton();
        btnAM = new javax.swing.JButton();
        lblFrecuencia = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(790, 530));
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        btnEncender.setBackground(new java.awt.Color(0, 255, 0));
        btnEncender.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        btnEncender.setText("l");
        btnEncender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEncenderActionPerformed(evt);
            }
        });

        btnApagar.setBackground(new java.awt.Color(255, 0, 0));
        btnApagar.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        btnApagar.setText("O");
        btnApagar.setEnabled(false);
        btnApagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnApagarActionPerformed(evt);
            }
        });

        pnlRadio.setEnabled(false);

        btnAvanzar.setBackground(new java.awt.Color(153, 153, 255));
        btnAvanzar.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        btnAvanzar.setText(">");
        btnAvanzar.setMaximumSize(new java.awt.Dimension(57, 45));
        btnAvanzar.setMinimumSize(new java.awt.Dimension(57, 45));
        btnAvanzar.setPreferredSize(new java.awt.Dimension(65, 65));
        btnAvanzar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAvanzarActionPerformed(evt);
            }
        });

        B12.setBackground(new java.awt.Color(204, 255, 255));
        B12.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        B12.setText("12");
        B12.setMaximumSize(new java.awt.Dimension(57, 45));
        B12.setMinimumSize(new java.awt.Dimension(57, 45));
        B12.setPreferredSize(new java.awt.Dimension(65, 65));
        B12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                B12MouseClicked(evt);
            }
        });

        btnRetroceder.setBackground(new java.awt.Color(153, 153, 255));
        btnRetroceder.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        btnRetroceder.setText("<");
        btnRetroceder.setMaximumSize(new java.awt.Dimension(57, 45));
        btnRetroceder.setMinimumSize(new java.awt.Dimension(57, 45));
        btnRetroceder.setPreferredSize(new java.awt.Dimension(65, 65));
        btnRetroceder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRetrocederActionPerformed(evt);
            }
        });

        B10.setBackground(new java.awt.Color(204, 255, 255));
        B10.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        B10.setText("10");
        B10.setMaximumSize(new java.awt.Dimension(57, 45));
        B10.setMinimumSize(new java.awt.Dimension(57, 45));
        B10.setPreferredSize(new java.awt.Dimension(65, 65));
        B10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                B10MouseClicked(evt);
            }
        });

        B11.setBackground(new java.awt.Color(204, 255, 255));
        B11.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        B11.setText("11");
        B11.setMaximumSize(new java.awt.Dimension(57, 45));
        B11.setMinimumSize(new java.awt.Dimension(57, 45));
        B11.setPreferredSize(new java.awt.Dimension(65, 65));
        B11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                B11MouseClicked(evt);
            }
        });

        B8.setBackground(new java.awt.Color(204, 255, 255));
        B8.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        B8.setText("8");
        B8.setMaximumSize(new java.awt.Dimension(57, 45));
        B8.setMinimumSize(new java.awt.Dimension(57, 45));
        B8.setPreferredSize(new java.awt.Dimension(65, 65));
        B8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                B8MouseClicked(evt);
            }
        });

        B9.setBackground(new java.awt.Color(204, 255, 255));
        B9.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        B9.setText("9");
        B9.setMaximumSize(new java.awt.Dimension(57, 45));
        B9.setMinimumSize(new java.awt.Dimension(57, 45));
        B9.setPreferredSize(new java.awt.Dimension(65, 65));
        B9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                B9MouseClicked(evt);
            }
        });

        B7.setBackground(new java.awt.Color(204, 255, 255));
        B7.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        B7.setText("7");
        B7.setMaximumSize(new java.awt.Dimension(57, 45));
        B7.setMinimumSize(new java.awt.Dimension(57, 45));
        B7.setPreferredSize(new java.awt.Dimension(65, 65));
        B7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                B7MouseClicked(evt);
            }
        });

        B4.setBackground(new java.awt.Color(204, 255, 255));
        B4.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        B4.setText("4");
        B4.setMaximumSize(new java.awt.Dimension(57, 45));
        B4.setMinimumSize(new java.awt.Dimension(57, 45));
        B4.setPreferredSize(new java.awt.Dimension(65, 65));
        B4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                B4MouseClicked(evt);
            }
        });

        B3.setBackground(new java.awt.Color(204, 255, 255));
        B3.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        B3.setText("3");
        B3.setMaximumSize(new java.awt.Dimension(57, 45));
        B3.setMinimumSize(new java.awt.Dimension(57, 45));
        B3.setPreferredSize(new java.awt.Dimension(65, 65));
        B3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                B3MouseClicked(evt);
            }
        });

        B2.setBackground(new java.awt.Color(204, 255, 255));
        B2.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        B2.setText("2");
        B2.setMaximumSize(new java.awt.Dimension(57, 45));
        B2.setMinimumSize(new java.awt.Dimension(57, 45));
        B2.setPreferredSize(new java.awt.Dimension(65, 65));
        B2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                B2MouseClicked(evt);
            }
        });

        B1.setBackground(new java.awt.Color(204, 255, 255));
        B1.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        B1.setText("1");
        B1.setMaximumSize(new java.awt.Dimension(57, 45));
        B1.setMinimumSize(new java.awt.Dimension(57, 45));
        B1.setPreferredSize(new java.awt.Dimension(65, 65));
        B1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                B1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                B1MouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                B1MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                B1MouseReleased(evt);
            }
        });
        B1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B1ActionPerformed(evt);
            }
        });
        B1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B1KeyPressed(evt);
            }
        });

        B6.setBackground(new java.awt.Color(204, 255, 255));
        B6.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        B6.setText("6");
        B6.setMaximumSize(new java.awt.Dimension(57, 45));
        B6.setMinimumSize(new java.awt.Dimension(57, 45));
        B6.setPreferredSize(new java.awt.Dimension(65, 65));
        B6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                B6MouseClicked(evt);
            }
        });

        B5.setBackground(new java.awt.Color(204, 255, 255));
        B5.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        B5.setText("5");
        B5.setMaximumSize(new java.awt.Dimension(57, 45));
        B5.setMinimumSize(new java.awt.Dimension(57, 45));
        B5.setPreferredSize(new java.awt.Dimension(65, 65));
        B5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                B5MouseClicked(evt);
            }
        });

        lblPantalla.setFont(new java.awt.Font("Century Gothic", 0, 48)); // NOI18N
        lblPantalla.setText("jLabel1");

        btnFM.setBackground(new java.awt.Color(255, 255, 204));
        btnFM.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        btnFM.setText("FM");
        btnFM.setEnabled(false);
        btnFM.setMaximumSize(new java.awt.Dimension(57, 45));
        btnFM.setMinimumSize(new java.awt.Dimension(57, 45));
        btnFM.setPreferredSize(new java.awt.Dimension(65, 65));
        btnFM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFMActionPerformed(evt);
            }
        });

        btnAM.setBackground(new java.awt.Color(255, 255, 204));
        btnAM.setFont(new java.awt.Font("Century Gothic", 1, 28)); // NOI18N
        btnAM.setText("AM");
        btnAM.setMaximumSize(new java.awt.Dimension(57, 45));
        btnAM.setMinimumSize(new java.awt.Dimension(57, 45));
        btnAM.setPreferredSize(new java.awt.Dimension(65, 65));
        btnAM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAMActionPerformed(evt);
            }
        });

        lblFrecuencia.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        lblFrecuencia.setText("FM");

        javax.swing.GroupLayout pnlRadioLayout = new javax.swing.GroupLayout(pnlRadio);
        pnlRadio.setLayout(pnlRadioLayout);
        pnlRadioLayout.setHorizontalGroup(
            pnlRadioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRadioLayout.createSequentialGroup()
                .addGroup(pnlRadioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRadioLayout.createSequentialGroup()
                        .addGap(98, 98, 98)
                        .addComponent(btnAM, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addComponent(btnFM, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlRadioLayout.createSequentialGroup()
                        .addGap(140, 140, 140)
                        .addComponent(lblPantalla)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblFrecuencia)
                        .addGap(22, 22, 22)
                        .addComponent(btnRetroceder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(btnAvanzar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlRadioLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(B1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addComponent(B2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addComponent(B3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addComponent(B4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addComponent(B5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addComponent(B6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlRadioLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(B7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addComponent(B8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addComponent(B9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addComponent(B10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(B11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(B12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(62, 62, 62))
        );
        pnlRadioLayout.setVerticalGroup(
            pnlRadioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRadioLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(pnlRadioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(71, 71, 71)
                .addGroup(pnlRadioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPantalla)
                    .addGroup(pnlRadioLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(pnlRadioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnRetroceder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAvanzar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(lblFrecuencia))
                .addGap(40, 40, 40)
                .addGroup(pnlRadioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(B1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlRadioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(B7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlRadioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(B10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(B11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(B12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnApagar, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEncender, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(68, 68, 68)
                .addComponent(pnlRadio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(btnEncender, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnApagar, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlRadio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    public void guardarFrecuencia(String fre, float est, int clic){
        
    }
    
    private void btnApagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnApagarActionPerformed
        miRadio.onOff();
        btnEncender.setEnabled(true);
        btnApagar.setEnabled(false);
        pnlRadio.setVisible(false);
    }//GEN-LAST:event_btnApagarActionPerformed

    private void btnAvanzarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAvanzarActionPerformed
        frecuencia = miRadio.siguiente(frecuencia);
        String a = String.valueOf(frecuencia);
        lblPantalla.setText(a);
    }//GEN-LAST:event_btnAvanzarActionPerformed

    private void btnRetrocederActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRetrocederActionPerformed
        frecuencia = miRadio.anterior(frecuencia);
        String a = String.valueOf(frecuencia);
        lblPantalla.setText(a);
    }//GEN-LAST:event_btnRetrocederActionPerformed
    
    private void B1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1MousePressed
        //int clicks = 0;
        //clicks = clicks + 1;
        
        //System.out.println(clicks + " clicks");
        
    }//GEN-LAST:event_B1MousePressed

    private void B1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B1ActionPerformed
        
    }//GEN-LAST:event_B1ActionPerformed

    private void B1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1MouseClicked
        if(evt.getClickCount() == 2){
            miRadio.guardar(frecuencia, 1);
            JOptionPane.showMessageDialog(this, "Se ha guardado: " + frecuencia + " " + estacion);
        }else if(evt.getClickCount() == 1){
            float num = miRadio.seleccionarFav(1);
            if(num != 0.0){
                frecuencia = num;
                texto = (String.valueOf(frecuencia));
                lblPantalla.setText(texto);
            }
        }
    }//GEN-LAST:event_B1MouseClicked

    private void B1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1MouseEntered
        
    }//GEN-LAST:event_B1MouseEntered

    private void B1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B1MouseReleased
        
    }//GEN-LAST:event_B1MouseReleased

    private void B1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B1KeyPressed
        /*int tecla = evt.getKeyCode();
        
        if(tecla == 71){
            
        }
        System.out.println(tecla);*/
    }//GEN-LAST:event_B1KeyPressed

    private void B2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B2MouseClicked
        if(evt.getClickCount() == 2){
            miRadio.guardar(frecuencia, 2);
            JOptionPane.showMessageDialog(this, "Se ha guardado: " + frecuencia + " " + estacion);
        }else if(evt.getClickCount() == 1){
            float num = miRadio.seleccionarFav(2);
            if(num != 0.0){
                frecuencia = num;
                texto = (String.valueOf(frecuencia));
                lblPantalla.setText(texto);
            }
        }
    }//GEN-LAST:event_B2MouseClicked

    private void B3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B3MouseClicked
        if(evt.getClickCount() == 2){
            miRadio.guardar(frecuencia, 3);
            JOptionPane.showMessageDialog(this, "Se ha guardado: " + frecuencia + " " + estacion);
        }else if(evt.getClickCount() == 1){
            float num = miRadio.seleccionarFav(3);
            if(num != 0.0){
                frecuencia = num;
                texto = (String.valueOf(frecuencia));
                lblPantalla.setText(texto);
            }
        }
    }//GEN-LAST:event_B3MouseClicked

    private void B4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B4MouseClicked
        if(evt.getClickCount() == 2){
            miRadio.guardar(frecuencia, 4);
            JOptionPane.showMessageDialog(this, "Se ha guardado: " + frecuencia + " " + estacion);
        }else if(evt.getClickCount() == 1){
            float num = miRadio.seleccionarFav(4);
            if(num != 0.0){
                frecuencia = num;
                texto = (String.valueOf(frecuencia));
                lblPantalla.setText(texto);
            }
        }
    }//GEN-LAST:event_B4MouseClicked

    private void B5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B5MouseClicked
        if(evt.getClickCount() == 2){
            miRadio.guardar(frecuencia, 5);
            JOptionPane.showMessageDialog(this, "Se ha guardado: " + frecuencia + " " + estacion);
        }else if(evt.getClickCount() == 1){
            float num = miRadio.seleccionarFav(5);
            if(num != 0.0){
                frecuencia = num;
                texto = (String.valueOf(frecuencia));
                lblPantalla.setText(texto);
            }
        }
    }//GEN-LAST:event_B5MouseClicked

    private void B6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B6MouseClicked
        if(evt.getClickCount() == 2){
            miRadio.guardar(frecuencia, 6);
            JOptionPane.showMessageDialog(this, "Se ha guardado: " + frecuencia + " " + estacion);
        }else if(evt.getClickCount() == 1){
            float num = miRadio.seleccionarFav(6);
            if(num != 0.0){
                frecuencia = num;
                texto = (String.valueOf(frecuencia));
                lblPantalla.setText(texto);
            }
        }
    }//GEN-LAST:event_B6MouseClicked

    private void B7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B7MouseClicked
        if(evt.getClickCount() == 2){
            miRadio.guardar(frecuencia, 7);
            JOptionPane.showMessageDialog(this, "Se ha guardado: " + frecuencia + " " + estacion);
        }else if(evt.getClickCount() == 1){
            float num = miRadio.seleccionarFav(7);
            if(num != 0.0){
                frecuencia = num;
                texto = (String.valueOf(frecuencia));
                lblPantalla.setText(texto);
            }
        }
    }//GEN-LAST:event_B7MouseClicked

    private void B8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B8MouseClicked
        if(evt.getClickCount() == 2){
            miRadio.guardar(frecuencia, 8);
            JOptionPane.showMessageDialog(this, "Se ha guardado: " + frecuencia + " " + estacion);
        }else if(evt.getClickCount() == 1){
            float num = miRadio.seleccionarFav(8);
            if(num != 0.0){
                frecuencia = num;
                texto = (String.valueOf(frecuencia));
                lblPantalla.setText(texto);
            }
        }
    }//GEN-LAST:event_B8MouseClicked

    private void B9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B9MouseClicked
        if(evt.getClickCount() == 2){
            miRadio.guardar(frecuencia, 9);
        }else if(evt.getClickCount() == 1){
            float num = miRadio.seleccionarFav(9);
            if(num != 0.0){
                frecuencia = num;
                texto = (String.valueOf(frecuencia));
                lblPantalla.setText(texto);
            }
        }
    }//GEN-LAST:event_B9MouseClicked

    private void B10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B10MouseClicked
        if(evt.getClickCount() == 2){
            miRadio.guardar(frecuencia, 10);
            JOptionPane.showMessageDialog(this, "Se ha guardado: " + frecuencia + " " + estacion);
        }else if(evt.getClickCount() == 1){
            float num = miRadio.seleccionarFav(10);
            if(num != 0.0){
                frecuencia = num;
                texto = (String.valueOf(frecuencia));
                lblPantalla.setText(texto);
            }
        }
    }//GEN-LAST:event_B10MouseClicked

    private void B11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B11MouseClicked
        if(evt.getClickCount() == 2){
            miRadio.guardar(frecuencia, 11);
            JOptionPane.showMessageDialog(this, "Se ha guardado: " + frecuencia + " " + estacion);
        }else if(evt.getClickCount() == 1){
            float num = miRadio.seleccionarFav(11);
            if(num != 0.0){
                frecuencia = num;
                texto = (String.valueOf(frecuencia));
                lblPantalla.setText(texto);
            }
        }
    }//GEN-LAST:event_B11MouseClicked

    private void B12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B12MouseClicked
        if(evt.getClickCount() == 2){
            miRadio.guardar(frecuencia, 12);
            JOptionPane.showMessageDialog(this, "Se ha guardado: " + frecuencia + " " + estacion);
        }else if(evt.getClickCount() == 1){
            float num = miRadio.seleccionarFav(12);
            if(num != 0.0){
                frecuencia = num;
                texto = (String.valueOf(frecuencia));
                lblPantalla.setText(texto);
            }
        }
    }//GEN-LAST:event_B12MouseClicked

    private void btnEncenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEncenderActionPerformed
        miRadio.onOff();
        btnApagar.setEnabled(true);
        btnEncender.setEnabled(false);
        pnlRadio.setVisible(true);
    }//GEN-LAST:event_btnEncenderActionPerformed

    private void btnAMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAMActionPerformed
        frecuencia = miRadio.Switch();
        texto = String.valueOf(frecuencia);
        lblPantalla.setText(texto);
        estacion = "AM";
        lblFrecuencia.setText(estacion);
        btnAM.setEnabled(false);
        btnFM.setEnabled(true);
    }//GEN-LAST:event_btnAMActionPerformed

    private void btnFMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFMActionPerformed
        frecuencia = miRadio.Switch();
        texto = String.valueOf(frecuencia);
        lblPantalla.setText(texto);
        estacion = "FM";
        lblFrecuencia.setText(estacion);
        btnAM.setEnabled(true);
        btnFM.setEnabled(false);
    }//GEN-LAST:event_btnFMActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        //System.out.println("Hola");
    }//GEN-LAST:event_formKeyPressed
       
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUI_Radio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUI_Radio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUI_Radio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUI_Radio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUI_Radio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B1;
    private javax.swing.JButton B10;
    private javax.swing.JButton B11;
    private javax.swing.JButton B12;
    private javax.swing.JButton B2;
    private javax.swing.JButton B3;
    private javax.swing.JButton B4;
    private javax.swing.JButton B5;
    private javax.swing.JButton B6;
    private javax.swing.JButton B7;
    private javax.swing.JButton B8;
    private javax.swing.JButton B9;
    private javax.swing.JButton btnAM;
    private javax.swing.JButton btnApagar;
    private javax.swing.JButton btnAvanzar;
    private javax.swing.JButton btnEncender;
    private javax.swing.JButton btnFM;
    private javax.swing.JButton btnRetroceder;
    private javax.swing.JLabel lblFrecuencia;
    private javax.swing.JLabel lblPantalla;
    private javax.swing.JPanel pnlRadio;
    // End of variables declaration//GEN-END:variables
}
