
/**
 *douglas.java 
 * Interfaz que tiene los siguientes métodos
 * @author Paul Belches
 * @author Javier Carpio
 * @since 14/01/18
 */
public interface douglas {

    /**
     * Método de encendido y/o apagado para la radio.
     */
    public void onOff();

    /**
     * Método para cambiar entre frecuencia Am/FM.
     * @return el valor de la nueva estación despúes del cambio
     */
    public float Switch();

    /**
     * Método para cambiar a la siguiente frecuencia. 
     * @param a frecuencia actual
     * @return siguiente frecuencia
     */
    public float siguiente(float a);

    /**
     * Método para cambiar a la frecuencia anterior.
     * @param a frecuencia actual
     * @return frecuencia anterior
     */
    public float anterior(float a);

    /**
     * Método para guardar una estación en alguno de los botones.
     * @param e Estación a guardar
     * @param b Botón en donde se va a guardar 
     */
    public void guardar(float e,int b);

    /**
     * Selecciónar una de las estaciones que se guardo.
     * @param b Boton de donde se quiere obtener la estación
     * @return La estación guardada en el botón
     */
    public float seleccionarFav(int b);
}
