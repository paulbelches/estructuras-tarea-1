
import java.text.DecimalFormat;

/**
 * @author Paul Belches
 * @author Javier Carpio
 * @since 14/01/18
 */
public class Radio implements douglas {
    private boolean eP, eE;
    private float fm,am;
    private float[][] b = new float[2][12];
    private DecimalFormat df;
    /**
     * Get para el estado de prendido.
     * @return El estado de prendido, true para encendido y false para apagado.
     */
    public boolean iseP() {
        return eP;
    }
    /**
     * Set para el estado de prendido.
     * @param El estado que se quiere establecer. 
     */
    public void seteP(boolean eP) {
        this.eP = eP;
    }
    /**
     * Get para el estado de estacion.
     * @return El estado de estacion, true para AM y false para FM.
     */
    public boolean iseE() {
        return eE;
    }
    /**
     * Set para el estado de estacion.
     * @param El estado que se quiere establecer. 
     */
    public void seteE(boolean eE) {
        this.eE = eE;
    }
    /**
     * Get para la estacion FM.
     * @return El valor de la estacion.
     */
    public float getFm() {
        return fm;
    }
    /**
     * Set para el valor de la frecuencia FM.
     * @param El valor de la frecuencia. 
     */
    public void setFm(float fm) {
        this.fm = fm;
    }
    /**
     * Get para la estacion AM.
     * @return El valor de la estacion.
     */
    public float getAm() {
        return am;
    }
    /**
     * Set para el valor de la frecuencia AM.
     * @param El valor de la frecuencia. 
     */
    public void setAm(float am) {
        this.am = am;
    }
    /**
     * Get para las estaciones guardadas.
     * @return La matriz con las estaciones guardadas.
     */
    public float[][] getB() {
        return b;
    }
    /**
     * Set para las frecuencias guardadas.
     * @param Matriz con todas las frecuencias guardadas. 
     */
    public void setB(float[][] b) {
        this.b = b;
    }
    /**
     * Metodo que nos permite cambiar de frecuencia.
     * @return Estacion asignada al momento de cambiar frecuecia.
     */
    public DecimalFormat getDf() {
        return df;
    }

    public void setDf(DecimalFormat df) {
        this.df = df;
    }
    
    /**
     * Constructor que permite crear los objetos de la clase Radio.
     */
    public Radio() {
        eP = true; // False es para apagado
        eE = false;// False es para fm
        am = 530;
        fm = (float)87.9;
        for (int j = 0; j < 2; j++){
            for (int i = 0; i < 12; i++){
                b[j][i] = 0;
            }
        }
        df  = new DecimalFormat("#.0");
    }
    
    /**
     * Metodo que nos permite encender y apagar la radio.
     */
    @Override
    public void onOff() {
         eP = !(eP);
     }
    
    /**
     * Metodo que nos permite cambiar de frecuencia.
     * @return Estacion asignada al momento de cambiar frecuecia.
     */
    public float Switch(){
        eE = !(eE);
        return (eE)?am:fm;
    }
    
    /**
     * Metodo que nos permite pasar a la siguiente estacion.
     * @param a Estacion actual.
     * @return Estacion siguiente.
     */
    @Override
    public float siguiente(float a){
        if (a > 108) {
            am = (am == 1610)? 530:a + 10;
            return am;
        } else {
            fm = (fm > 107.8)? (float)87.9: a + (float)0.200;
            fm = Float.parseFloat(df.format(fm));
            return fm;
        }
    }
    
    /**
     * Metodo que nos permite pasar a la estacion anterior.
     * @param a Estacion actual.
     * @return Estacion anterior.
     */
    @Override
    public float anterior(float a){
         if (a > 108) {
            am = (am == 530)? 1610:a - 10;
            return am;
        } else {
            fm = (fm < 88.0)? (float)107.9: a - (float)0.200;
            fm = Float.parseFloat(df.format(fm));
            return fm;
        }
    }
    
    /**
     * Metodo que nos permite guardar una estacion favorita, independientemente de la frecuencia.
     * @param e Estacion favorita.
     * @param b Numero del boton donde se guardara.
     */
    @Override
    public void guardar(float e,int b){
        int n = (e>108)? 1:0;
        this.b[n][b] = e; 
    }
    
    /**
     * Metodo que nos permite conocer cual es la estacion guardada del boton seleccionado.
     * @param b Numero del boton presionado.
     * @return Estacion favorita.
     */
    @Override
    public float seleccionarFav(int b){
        if (!eE) {
            if(this.b[0][b] > 1){
                fm =  this.b[0][b];
            }
            
            return this.b[0][b];
        } else {
            if(this.b[1][b] > 1){
                am =  this.b[1][b];
            }
            
            return this.b[1][b];
        }
    }
}
