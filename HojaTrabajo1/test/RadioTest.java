/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author javie
 */
public class RadioTest {
    
    public RadioTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of onOff method, of class Radio.
     */
    @org.junit.Test
    public void testOnOff() {
        System.out.println("onOff");
        Radio instance = new RadioImpl();
        instance.onOff();
        assertEquals(false, instance.iseP());
    }

    /**
     * Test of Switch method, of class Radio.
     */
    @org.junit.Test
    public void testSwitch() {
        System.out.println("Switch");
        Radio instance = new RadioImpl();
        float expResult = instance.getFm();
        float result = instance.Switch();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of siguiente method, of class Radio.
     */
    @org.junit.Test
    public void testSiguiente() {
        System.out.println("siguiente");
        Radio instance = new RadioImpl();
        float expResult = 540;
        float result = instance.siguiente(530);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of anterior method, of class Radio.
     */
    @org.junit.Test
    public void testAnterior() {
        System.out.println("siguiente");
        Radio instance = new RadioImpl();
        float expResult = 1610;
        float result = instance.anterior(530);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of guardar method, of class Radio.
     */
    @org.junit.Test
    public void testGuardar() {
        System.out.println("guardar");
        Radio instance = new RadioImpl();
        float expResult = 530;
        instance.guardar(530, 3);
        assertEquals(expResult, instance.getB()[1][3], 0.0);
    }

    /**
     * Test of seleccionarFav method, of class Radio.
     */
    @org.junit.Test
    public void testSeleccionarFav() {
        System.out.println("seleccionarFav");
        Radio instance = new RadioImpl();
        instance.guardar(530, 3);
        float result = instance.seleccionarFav(3);
        assertEquals(530, result, 0.0);
    }

    public class RadioImpl extends Radio {
    }
    
}
